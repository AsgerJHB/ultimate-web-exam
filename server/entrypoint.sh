#!/bin/sh

start_server() {
    echo "Starting Bottle server..."
    python -m bottle --server paste --bind 0.0.0.0:4000 --debug --reload app &
    SERVER_PID=$!
    echo "Bottle server started with PID $SERVER_PID"
}

stop_server() {
    echo "Stopping Bottle server..."
    kill $SERVER_PID
    echo "Bottle server stopped"
}

case "$RTE" in
    dev )
        echo "** Development mode."
        python -m bottle --server paste --bind 0.0.0.0:4000 --debug --reload app
        ;;
    test )
        echo "** Test mode."
        pip-audit
        start_server
        # Allow the server some time to start
        sleep 5
        # Run tests with pytest and measure coverage
        pytest -s --cov=./ --cov-report term-missing --cov-config=./omit_py_files.toml --disable-pytest-warnings ./test_api.py
        TEST_RESULT=$?
        stop_server
        exit $TEST_RESULT
        ;;
    prod )
        echo "** Production mode."
        pip-audit || exit 1
        python manage.py check --deploy
        ;;
esac